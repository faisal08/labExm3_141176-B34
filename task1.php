<?php
$start=1;
$end=1000;
$fp=fopen("PrimeNumbers_141176","w+");
$count=1;
for($i=$start;$i<=$end;$i++)
{
    if($i==1)continue;
    $isPrime=true;
    for($j=2;$j<=ceil($i/2);$j++)
    {
        if($i%$j==0)
        {
            $isPrime=false;
            break;
        }
    }
    if($isPrime)
    {
        //echo $i."  ";
         fwrite($fp,$i." ");
        if($count==20) {
        fwrite($fp, $i . "\n ");
         $count =0;
    }
    $count++;
     }
}

fclose($fp);